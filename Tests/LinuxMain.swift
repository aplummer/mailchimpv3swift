import XCTest
@testable import MailChimpV3SwiftTests

XCTMain([
    testCase(MailChimpV3SwiftTests.allTests),
])
