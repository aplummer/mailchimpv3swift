import XCTest
@testable import MailChimpV3Swift

class MailChimpV3SwiftTests: XCTestCase {
    
    func generateAPI() -> MailChimpV3API {
        
        guard let api = try? MailChimpV3API("") else {
            XCTFail("Unable to create API")
            fatalError()
        }
        
        return api
        
    }
    
    func testAddMember() {
        
        let expectation = XCTestExpectation(description: "Add a member to a list")
        
        let api = generateAPI()
        
        let createList = CreateListRequest(
            name: "test list",
            contact: ListContact(
                company: "Test Co",
                address1: "test address 1",
                address2: nil,
                city: "Melbourne",
                state: "Vic",
                zip: "3000",
                country: "AU",
                phone: nil
            ),
            permissionReminder: "You used API",
            useArchiveBar: nil,
            campaignDefaults: CampaignDefaults(
                fromName: "andrew",
                fromEmail: "andrewplummer@me.com",
                subject: "Test Subj",
                language: "en"
            ),
            notifyOnSubscribe: nil,
            notifyOnUnsubscribe: nil,
            emailTypeOption: true,
            visibility: CreateListRequest.Visibility.prv
        )
        
        do {
            
            api.create(list: createList) { response in
                
                switch response {
                    
                    case .success(let list):
                        let id = list.id
                        
                        let _member = MemberRequest(
                            emailAddress: "test@resolve.expert",
                            emailType: nil,
                            status: .subscribed,
                            mergeFields: nil,
                            language: "en",
                            vip: nil,
                            location: nil,
                            ipSignup: nil,
                            timestampSignup: nil,
                            ipOpt: nil,
                            timestampOpt: nil
                        )
                        
                        do {
                            
                            try api.addListMemberRequest(listId: id, member: _member) { response in
                                
                                switch response {
                                    
                                case .success(let member):
                                    
                                    print(member.id, member.emailAddress, member.status)
                                    
                                    expectation.fulfill()
                                    return
                                    
                                case .error(let error):
                                    
                                    XCTFail(error.title)
                                    return
                                    
                                }
                                
                            }
                            
                        } catch let error {
                            debugPrint(error)
                        }
                    
                    case .error(let error):
                        XCTFail()
                    }
                
            }
        } catch let error {
            XCTFail(error.localizedDescription)
        }
        
        
        wait(for: [expectation], timeout: 30.0)
        
    }
    
    func testGetLists() {
    
        let expectation = XCTestExpectation(description: "Get mailing lists")
    
        let api = generateAPI()
    
        do {
            try api.getLists { response in
                
                switch response {
                    
                case .success(let data):
                    
                    debugPrint("data:", data)
                    expectation.fulfill()
                    
                case .error(let error):
                    
                    XCTFail("Mailchimp error object: \(error.title)")
                    
                }
                
            }
        } catch let error {
            print(error)
            XCTFail(error.localizedDescription)
        }
        
        wait(for: [expectation], timeout: 30.0)
    
    }
    
    
    func testCreateList() {
        
        let expectation = XCTestExpectation(description: "Create mailing list")
        
        let api = generateAPI()
        
        let createList = CreateListRequest(
            name: "test list",
            contact: ListContact(
                company: "Test Co",
                address1: "test address 1",
                address2: nil,
                city: "Melbourne",
                state: "Vic",
                zip: "3000",
                country: "AU",
                phone: nil
            ),
            permissionReminder: "You used API",
            useArchiveBar: nil,
            campaignDefaults: CampaignDefaults(
                fromName: "andrew",
                fromEmail: "andrewplummer@me.com",
                subject: "Test Subj",
                language: "en"
            ),
            notifyOnSubscribe: nil,
            notifyOnUnsubscribe: nil,
            emailTypeOption: true,
            visibility: CreateListRequest.Visibility.prv
        )
        
        do {
            
            try api.create(list: createList) { response in
                
                switch response {
                    
                    case .success(let data):
                        
                        debugPrint("created at:", data.dateCreated)
                        expectation.fulfill()
                    
                    case .error(let error):
                        
                        XCTFail("Mailchimp error object: \(error.title)")
                    
                }
            }
            
        } catch let error {
            print(error)
            XCTFail(error.localizedDescription)
        }
        
        wait(for: [expectation], timeout: 30.0)
        
        XCTAssert(true)
    }


    static var allTests = [
        ("testCreateList", testCreateList),
    ]
}
