//
//  MailChimpV3Swift.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 29/1/18.
//

import Foundation

public protocol MailChimpV3APITraits {
    
    /// The client API Key, available at https://<dc>.admin.mailchimp.com/account/api/
    var apiKey: String { get }
    
    /// The base URL for the mailchimp API
    var baseUrl: URL { get }
    
    /// A default timeout for network requests
    var timeout: TimeInterval { get }
    
    /// A wrapping function to reduce boilerplate associated with using URLSession / URLRequest.
    ///
    /// - Parameters:
    ///   - urlRequest: The `URLRequest` to perform
    ///   - callback: A callback function to invoke for success / failure codes
    func getJSONResponse<T>(urlRequest: inout URLRequest, callback: ((APIResponse<T>) -> Void)?)
}

public struct MailChimpV3API: Lists, MailChimpV3APITraits {
    
    public let apiKey: String
    public let baseUrl: URL
    public let timeout: TimeInterval = 15
    
    public init(_ apiKey: String) throws {
        
        guard
            let dc = apiKey.split(separator: "-").last,
            let baseUrl = URL(string: "https://\(dc).api.mailchimp.com") else {
            throw ConfigError.unableToParseAPIKey
        }
        
        self.apiKey = apiKey
        self.baseUrl = baseUrl
        
    }
    
    public func getJSONResponse<T>(
        urlRequest: inout URLRequest,
        callback: ((APIResponse<T>) -> Void)? = nil) {

        let session = URLSession(configuration: .default)
        
        urlRequest.setMailChimpAuthHeader(apiKey: apiKey)
        
        urlRequest.setValue(
            "application/x-www-form-urlencoded",
            forHTTPHeaderField: "Content-Type"
        )
        
        let dataTask = session.dataTask(with: urlRequest) { (data, urlResponse, error) in
            
            guard error == nil else {
                callback?(.error(.generic))
                return
            }
            
            let decoder = JSONDecoder()
            
            guard let data = data else {
                callback?(.error(.generic))
                return
            }
            
            do {
                
                let response = try decoder.decode(T.self, from: data)
                callback?(.success(response))
                return
            } catch let error {
                debugPrint(error)
            }
            
            if let error = try? decoder.decode(APIError.self, from: data) {
                callback?(.error(error))
                return
            }
            
            callback?(.error(.generic))
            
        }
        
        dataTask.resume()
        
    }
    
}
