//
//  CampaignDefaults.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 29/1/18.
//

import Foundation

/// Default values for campaigns created for this list.
public struct CampaignDefaults: Codable {
    
    enum CodingKeys: String, CodingKey {
        
        case
            fromName = "from_name",
            fromEmail = "from_email",
            subject = "subject",
            language = "language"
        
    }
    
    /// The default from name for campaigns sent to this list.
    let fromName: String
    
    /// The default from email for campaigns sent to this list.
    let fromEmail: String
    
    /// The default subject line for campaigns sent to this list.
    let subject: String
    
    /// The default language for this lists’s forms.
    let language: String
    
}
