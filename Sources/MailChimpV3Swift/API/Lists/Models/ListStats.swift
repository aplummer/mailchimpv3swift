//
//  ListStats.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 29/1/18.
//

import Foundation

public struct ListStats: Codable {
    
    enum CodingKeys: String, CodingKey {
        case
            memberCount = "member_count",
            unsubscribeCount = "unsubscribe_count",
            cleanedCount = "cleaned_count",
            memberCountSinceSend = "member_count_since_send",
            unsubscribeCountSinceSend = "unsubscribe_count_since_send",
            cleanedCountSinceSend = "cleaned_count_since_send",
            campaignCount = "campaign_count",
            campaignLastSent = "campaign_last_sent",
            mergeFieldCount = "merge_field_count",
            avgSubRate = "avg_sub_rate",
            avgUnsubRate = "avg_unsub_rate",
            targetSubRate = "target_sub_rate",
            openRate = "open_rate",
            clickRate = "click_rate",
            lastSubDate = "last_sub_date",
            lastUnsubDate = "last_unsub_date"
    }
    
    /// The number of active members in the list.
    let memberCount: Int
    
    /// The number of members who have unsubscribed from the list.
    let unsubscribeCount: Int
    
    /// The number of members cleaned from the list.
    let cleanedCount: Int
    
    /// The number of active members in the list since the last campaign was sent.
    let memberCountSinceSend: Int
    
    /// The number of members who have unsubscribed since the last campaign was sent.
    let unsubscribeCountSinceSend: Int
    
    /// The number of members cleaned from the list since the last campaign was sent.
    let cleanedCountSinceSend: Int
    
    /// The number of campaigns in any status that use this list.
    let campaignCount: Int
    
    /// The date and time the last campaign was sent to this list. This is updated
    /// when a campaign is sent to 10 or more recipients.
    let campaignLastSent: String
    
    /// The number of merge vars for this list (not EMAIL, which is required).
    let mergeFieldCount: Int
    
    /// The average number of subscriptions per month for the list (not returned
    /// if we haven’t calculated it yet).
    let avgSubRate: Double
    
    /// The average number of unsubscriptions per month for the list
    /// (not returned if we haven’t calculated it yet).
    let avgUnsubRate: Double
    
    /// The target number of subscriptions per month for the list to keep it growing
    /// (not returned if we haven’t calculated it yet).
    let targetSubRate: Double
    
    /// The average open rate (a percentage represented as a number between 0 and 100)
    /// per campaign for the list (not returned if we haven’t calculated it yet).
    let openRate: Double
    
    /// The average click rate (a percentage represented as a number between 0 and 100)
    /// per campaign for the list (not returned if we haven’t calculated it yet).
    let clickRate: Double
    
    /// The date and time of the last time someone subscribed to this list.
    let lastSubDate: String
    
    /// The date and time of the last time someone unsubscribed from this list.
    let lastUnsubDate: String
    
}
