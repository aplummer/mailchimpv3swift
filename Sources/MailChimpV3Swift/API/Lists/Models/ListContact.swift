//
//  ListContact.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 29/1/18.
//

import Foundation

public struct ListContact: Codable {
    
    /// The company name for the list.
    let company: String
    
    /// The street address for the list contact.
    let address1: String
    
    /// The street address for the list contact.
    let address2: String?
    
    /// The city for the list contact.
    let city: String
    
    /// The state for the list contact.
    let state: String
    
    /// The postal or zip code for the list contact.
    let zip: String
    
    /// A two-character ISO3166 country code. Defaults to US if invalid.
    let country: String
    
    /// The phone number for the list contact.
    let phone: String?
}
