//
//  Lists.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 23/1/18.
//

import Foundation

public protocol Lists {
    
    /// Create a new list in your MailChimp account.
    ///
    /// - Parameter callback: Callback with API response
    func create(list: CreateListRequest, callback: APIResponseCallback<ListResponse>) throws
    
    /// Gets list/s with an optional ID filter.
    ///
    /// - Parameter id: Filter for a specific list
    /// - Throws: An error if the request fails
    func getLists(id: String?, callback: APIResponseCallback<ListsResponse>) throws
    
}

public extension Lists where Self: MailChimpV3APITraits {
    
    func addListMemberRequest(listId: String, member: MemberRequest, callback: APIResponseCallback<MemberResponse> = nil) throws {
        
        guard let url = URL(string: "/3.0/lists/\(listId)/members", relativeTo: baseUrl) else {
            callback?(.error(APIError.generic))
            return
        }
        
        var req = URLRequest(
            url: url,
            cachePolicy: .reloadIgnoringLocalCacheData,
            timeoutInterval: timeout
        )
        
        let encoder = JSONEncoder()
        
        guard let dataRep = try? encoder.encode(member) else {
            callback?(.error(APIError.generic))
            return
        }
        
        req.httpBody = dataRep
        req.httpMethod = HTTPMethod.post.rawValue
        
        getJSONResponse(urlRequest: &req, callback: callback)
        
    }
    
    func getLists(id: String? = nil, callback: APIResponseCallback<ListsResponse> = nil) throws {
        
        guard let url: URL = id == nil
            ? URL(string: "/3.0/lists", relativeTo: baseUrl)
            : URL(string: "/3.0/lists/\(id ?? "")", relativeTo: baseUrl) else {
                callback?(.error(APIError.generic))
                return
        }
        
        var req = URLRequest(
            url: url,
            cachePolicy: .reloadIgnoringLocalCacheData,
            timeoutInterval: timeout
        )
        
        req.httpMethod = HTTPMethod.get.rawValue
        
        getJSONResponse(urlRequest: &req, callback: callback)
        
    }
    
    func create(list: CreateListRequest, callback: APIResponseCallback<ListResponse> = nil) {
        
        guard let url = URL(string: "/3.0/lists", relativeTo: baseUrl) else {
            callback?(.error(APIError.generic))
            return
        }
        
        var req = URLRequest(
            url: url,
            cachePolicy: .reloadIgnoringLocalCacheData,
            timeoutInterval: timeout
        )
        
        let encoder = JSONEncoder()
        
        guard let dataRep = try? encoder.encode(list) else {
            callback?(.error(APIError.generic))
            return
        }
        
        req.httpBody = dataRep
        req.httpMethod = HTTPMethod.post.rawValue
        
        getJSONResponse(urlRequest: &req, callback: callback)
        
    }
    
}
