//
//  CreateListRequest.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 29/1/18.
//

import Foundation

/// The request object to create a list.
public struct CreateListRequest: Codable {
    
    enum Visibility: String, Codable {
        case
            pub,
            prv
    }
    
    enum CodingKeys: String, CodingKey {
        
        case
            name = "name",
            contact = "contact",
            permissionReminder = "permission_reminder",
            useArchiveBar = "use_archive_bar",
            campaignDefaults = "campaign_defaults",
            notifyOnSubscribe = "notify_on_subscribe",
            notifyOnUnsubscribe = "notify_on_unsubscribe",
            emailTypeOption = "email_type_option",
            visibility = "visibility"
    }
    
    /// The name of the list.
    let name: String
    
    /// Contact information displayed in campaign footers to
    /// comply with international spam laws.
    let contact: ListContact
    
    /// The permission reminder for the list.
    let permissionReminder: String
    
    /// Whether campaigns for this list use the Archive Bar
    /// in archives by default.
    let useArchiveBar: Bool?
    
    /// Default values for campaigns created for this list.
    let campaignDefaults: CampaignDefaults
    
    /// The email address to send subscribe notifications to.
    let notifyOnSubscribe: String?
    
    /// The email address to send unsubscribe notifications to.
    let notifyOnUnsubscribe: String?
    
    /// Whether the list supports multiple formats for emails.
    /// When set to true, subscribers can choose whether they want
    /// to receive HTML or plain-text emails. When set to false,
    /// subscribers will receive HTML emails, with a plain-text
    /// alternative backup.
    let emailTypeOption: Bool
    
    /// Whether this list is public or private.
    /// Possible Values:
    /// - pub
    /// - prv
    let visibility: Visibility?
    
}
