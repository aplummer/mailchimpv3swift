//
//  ListMember.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 29/1/18.
//

import Foundation

public struct MemberRequest: Codable {
    
    enum CodingKeys: String, CodingKey {
        case
            emailAddress = "email_address",
            emailType = "email_type",
            status = "status",
            mergeFields = "merge_fields",
            language = "language",
            vip = "vip",
            location = "location",
            ipSignup = "ip_signup",
            timestampSignup = "timestamp_signup",
            ipOpt = "ip_opt",
            timestampOpt = "timestamp_opt"
    }
    
    /// Email address for a subscriber.
    let emailAddress: String
    
    /// Type of email this member asked to get (‘html’ or ‘text’).
    let emailType: EmailType?
    
    /// Subscriber’s current status.
    let status: ListStatus
    
    /// An individual merge var and value for a member.
    let mergeFields: [String: String]?
    
    /// If set/detected, the subscriber’s language.
    let language: String?
    
    /// VIP status for subscriber.
    let vip: Bool?
    
    /// Subscriber location information.
    let location: Location?
    
    /// IP address the subscriber signed up from.
    let ipSignup: String?
    
    /// The date and time the subscriber signed up for the list.
    let timestampSignup: String?
    
    /// The IP address the subscriber used to confirm their opt-in status.
    let ipOpt: String?
    
    /// The date and time the subscribe confirmed their opt-in status.
    let timestampOpt: String?
    
    public init(
        emailAddress: String,
        emailType: EmailType?,
        status: ListStatus,
        mergeFields: [String: String]?,
        language: String?,
        vip: Bool?,
        location: Location?,
        ipSignup: String?,
        timestampSignup: String?,
        ipOpt: String?,
        timestampOpt: String?
        ) {
        
        self.emailAddress = emailAddress
        self.emailType = emailType
        self.status = status
        self.mergeFields = mergeFields
        self.language = language
        self.vip = vip
        self.location = location
        self.ipSignup = ipSignup
        self.timestampSignup = timestampSignup
        self.ipOpt = ipOpt
        self.timestampOpt = timestampOpt
        
    }
        
    
}
