//
//  MemberResponse.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 29/1/18.
//

import Foundation

public struct MemberResponse: Codable {
    
    enum CodingKeys: String, CodingKey {
        case
            id = "id",
            emailAddress = "email_address",
            uniqueEmailId = "unique_email_id",
            emailType = "email_type",
            status = "status",
            unsubscribeReason = "unsubscribe_reason",
            mergeFields = "merge_fields",
            language = "language",
            _links = "_links"
    }
    
    let id: String
    
    let emailAddress: String
    
    let uniqueEmailId: String
    
    let emailType: EmailType
    
    let status: ListStatus
    
    let unsubscribeReason: String?
    
    let mergeFields: [String: String]?
    
    let language: String?
    
    let _links: [Link]
    
}
