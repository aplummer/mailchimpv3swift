//
//  CreateListResponse.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 29/1/18.
//

import Foundation

/// The response object when a list has been created.
public struct ListResponse: Codable {
    
    enum CodingKeys: String, CodingKey {
        
        case
            id = "id",
            webId = "web_id",
            name = "name",
            contact = "contact",
            permissionReminder = "permission_reminder",
            useArchiveBar = "use_archive_bar",
            campaignDefaults = "campaign_defaults",
            notifyOnSubscribe = "notify_on_subscribe",
            notifyOnUnsubscribe = "notify_on_unsubscribe",
            dateCreated = "date_created",
            listRating = "list_rating",
            emailTypeOption = "email_type_option",
            subscribeUrlShort = "subscribe_url_short",
            subscribeUrlLong = "subscribe_url_long",
            beamerAddress = "beamer_address",
            visibility = "visibility",
            // modules = "modules",
            stats = "stats",
            _links = "_links"
        
    }
    
    /// A string that uniquely identifies this list.
    let id: String
    
    /// The ID used in the MailChimp web application. View this list in
    /// your MailChimp account at https://{dc}.admin.mailchimp.com/lists/members/?id={web_id}.
    let webId: Int
    
    /// A string that uniquely identifies this list.
    let name: String
    
    /// Contact information displayed in campaign footers to comply with international spam laws.
    let contact: ListContact
    
    /// The permission reminder for the list.
    let permissionReminder: String
    
    /// Whether campaigns for this list use the Archive Bar
    /// in archives by default.
    let useArchiveBar: Bool
    
    /// Default values for campaigns created for this list.
    let campaignDefaults: CampaignDefaults
    
    /// The email address to send subscribe notifications to.
    let notifyOnSubscribe: String
    
    /// The email address to send unsubscribe notifications to.
    let notifyOnUnsubscribe: String
    
    /// The date and time that this list was created.
    let dateCreated: String
    
    /// An auto-generated activity score for the list (0-5).
    let listRating: Int
    
    /// Whether the list supports multiple formats for emails.
    /// When set to true, subscribers can choose whether they want
    /// to receive HTML or plain-text emails. When set to false,
    /// subscribers will receive HTML emails, with a plain-text
    /// alternative backup.
    let emailTypeOption: Bool
    
    /// Our EepURL shortened version of this list’s subscribe form.
    let subscribeUrlShort: String
    
    /// The full version of this list’s subscribe form (host will vary).
    let subscribeUrlLong: String
    
    /// The list’s Email Beamer address.
    let beamerAddress: String
    
    /// Whether this list is public or private.
    /// Possible Values:
    /// - pub
    /// - prv
    let visibility: String
    
    // TODO: Implement Modules
    //let modules: [String]
    
    /// Stats for the list. Many of these are cached for at least five minutes.
    let stats: ListStats
    
    /// A list of link types and descriptions for the API schema documents.
    let _links: [Link]
    
}
