//
//  ListsResponse.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 29/1/18.
//

import Foundation

public struct ListsResponse: Codable {
    
    enum CodingKeys: String, CodingKey {
        case
            lists = "lists",
            totalItems = "total_items",
            _links = "_links"
    }
    
    let lists: [ListResponse]
    let totalItems: Int
    let _links: [Link]
    
}
