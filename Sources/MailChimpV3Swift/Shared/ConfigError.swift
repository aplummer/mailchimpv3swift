//
//  ConfigError.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 29/1/18.
//

import Foundation

enum ConfigError: Error {
    
    case
        unableToParseAPIKey,
        internalError
    
}
