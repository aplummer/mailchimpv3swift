//
//  APIError.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 23/1/18.
//

import Foundation

public struct APIError: Error, Codable {
    
    let type: String
    let title: String
    let status: Int
    let detail: String
    let instance: String
    
    static var generic: APIError {
        return APIError(
            type: "Generic Error",
            title: "Generic Error",
            status: 500,
            detail: "There was an unexpected error in the MailChimpV3Swift package.",
            instance: "Generic Error"
        )
    }
    
}
