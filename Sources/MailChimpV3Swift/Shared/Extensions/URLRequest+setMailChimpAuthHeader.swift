//
//  URLRequest+SetAuthorizationHeader.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 29/1/18.
//

import Foundation

extension URLRequest {
    
    mutating func setMailChimpAuthHeader(apiKey: String) {
        
        let username = "user"
        
        let str = "\(username):\(apiKey)"
        
        let d = Data(str.utf8).base64EncodedString()
        
        setValue("Basic \(d)", forHTTPHeaderField: "Authorization")
        
    }
    
}
