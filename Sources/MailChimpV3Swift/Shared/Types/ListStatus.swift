//
//  ListStatus.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 29/1/18.
//

import Foundation

public enum ListStatus: String, Codable {
    case
        subscribed,
        unsubscribed,
        cleaned,
        pending,
        transactional
}
