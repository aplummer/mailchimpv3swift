//
//  HTTPMethod.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 29/1/18.
//

import Foundation

public enum HTTPMethod: String, Codable {
    
    case
        get = "GET",
        post = "POST",
        put = "PUT",
        patch = "PATCH",
        delete = "DELETE",
        options = "OPTIONS",
        head = "HEAD"
    
}
