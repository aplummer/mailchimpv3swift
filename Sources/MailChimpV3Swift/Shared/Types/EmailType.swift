//
//  EmailType.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 29/1/18.
//

import Foundation

public enum EmailType: String, Codable {
    case
    html,
    text
}
