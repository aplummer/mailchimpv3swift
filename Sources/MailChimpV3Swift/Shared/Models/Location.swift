//
//  Location.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 29/1/18.
//

import Foundation

/// Subscriber location information.
public struct Location: Codable {
    
    /// The location latitude.
    var latitude: Double
    
    /// The location longitude.
    var longitude: Double
    
}
