//
//  Link.swift
//  MailChimpV3Swift
//
//  Created by Andrew Plummer on 29/1/18.
//

import Foundation

public struct Link: Codable {
    
    /// As with an HTML ‘rel’ attribute, this describes the type of link.
    let rel: String
    
    /// This property contains a fully-qualified URL that can be called to retrieve the linked resource or perform the linked action.
    let href: String
    
    /// The HTTP method that should be used when accessing the URL defined in ‘href’.
    /// Possible Values:
    /// - GET
    /// - POST
    /// - PUT
    /// - PATCH
    /// - DELETE
    /// - OPTIONS
    /// - HEAD
    let method: HTTPMethod
    
    /// For GETs, this is a URL representing the schema that the response should conform to.
    let targetSchema: String?
    
    /// For HTTP methods that can receive bodies (POST and PUT),
    /// this is a URL representing the schema that the body should conform to.
    let schema: String?
    
}
